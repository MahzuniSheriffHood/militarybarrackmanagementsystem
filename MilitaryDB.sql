create database Military
go
use Military
create table Soldiers(ID int identity(1,1) primary key, FirstName nvarchar(32) NOT NULL, LastName nvarchar(32) NOT NULL, Status nvarchar(32) NOT NULL, RankID int NOT NULL, TeamID int NOT NULL, DateOfBirth date NOT NULL, StartingDate date NOT NULL)
create table Vehicles(ID int primary key NOT NULL, Name nvarchar(100) NOT NULL, VehicleCategoryID int NOT NULL, BelongingUnitID int, UsingUnitID int, Status nvarchar(32) NOT NULL)
create table Weapons(ID int primary key NOT NULL, Name nvarchar(100) NOT NULL, WeaponCategoryID int NOT NULL, BelongingSoldierID int, Status nvarchar(32) NOT NULL)
create table Arsenal(ID int identity(1,1) primary key, Name nvarchar(100) NOT NULL, Quantity int NOT NULL)
create table WeaponCategories(ID int identity(1,1) primary key, Name nvarchar(32) NOT NULL)
create table VehicleCategories(ID int identity(1,1) primary key, Name nvarchar(32) NOT NULL)
create table DisciplinaryPunishments(ID int identity(1,1) primary key, AppliedSoldierID int NOT NULL, Date date, Description nvarchar(250) NOT NULL)
create table Equipments(ID int primary key NOT NULL, Name nvarchar(100), BelongingSoldierID int, Status nvarchar(32) NOT NULL)
create table ArsenalUsages(UsedUnitID int NOT NULL, UsedArsenalID int NOT NULL, Quantity int NOT NULL, OperationID int NOT NULL)
create table UnitTypes(ID int identity(1,1) primary key, Name nvarchar(32) NOT NULL)
create table Operations(ID int identity(1,1) primary key, Name nvarchar(100) NOT NULL, StartingDate date NOT NULL, Time time NOT NULL, ReportDirectory nvarchar(500) NOT NULL)
create table UnitsInOperations(OperationID int NOT NULL, UnitID int NOT NULL)
create table Units(ID int identity(1,1) primary key, UnitTypeID int NOT NULL, ParentUnitID int, Name nvarchar(100) NOT NULL, LeaderID int)
create table Ranks(ID int identity(1,1) primary key, Name nvarchar(32) NOT NULL)
go
alter table Soldiers add constraint FK_Rank foreign key (RankID) references Ranks(ID)
alter table Soldiers add constraint FK_Team foreign key (TeamID) references Units(ID)
alter table Vehicles add constraint FK_VehicleCategory foreign key (VehicleCategoryID) references VehicleCategories(ID)
alter table Vehicles add constraint FK_BelongingUnit foreign key (BelongingUnitID) references Units(ID)
alter table Vehicles add constraint FK_UsingUnit foreign key (UsingUnitID) references Units(ID)
alter table Weapons add constraint FK_WeaponCategory foreign key (WeaponCategoryID) references WeaponCategories(ID)
alter table Weapons add constraint FK_UsingSoldier foreign key (BelongingSoldierID) references Soldiers(ID)
alter table DisciplinaryPunishments add constraint FK_AppliedSoldier foreign key (AppliedSoldierID) references Soldiers(ID)
alter table Equipments add constraint FK_BelongingSoldier foreign key (BelongingSoldierID) references Soldiers(ID)
alter table ArsenalUsages add constraint CPK_JunctionTableArsenalUsage primary key (UsedUnitID, UsedArsenalID, OperationID)
alter table ArsenalUsages add constraint FK_UsedUnit foreign key (UsedUnitID) references Units(ID)
alter table ArsenalUsages add constraint FK_UsedArsenal foreign key (UsedArsenalID) references Arsenal(ID)
alter table ArsenalUsages add constraint FK_ArsenalUsedInOperation foreign key (OperationID) references Operations(ID)
alter table UnitsInOperations add constraint CPK_JunctionTableUnitsInOperations primary key (OperationID, UnitID)
alter table UnitsInOperations add constraint FK_UnitsTookPlaceInOperation foreign key (OperationID) references Operations(ID)
alter table UnitsInOperations add constraint FK_Unit foreign key (UnitID) references Units(ID)
alter table Units add constraint FK_UnitType foreign key (UnitTypeID) references UnitTypes(ID)
alter table Units add constraint FK_ParentUnit foreign key (ParentUnitID) references Units(ID)
alter table Units add constraint FK_Leader foreign key (LeaderID) references Soldiers(ID)
go
create login HulusiAkar with password = '123456'
go
create user GenelkurmayBaskani for login HulusiAkar
go
grant select, update, insert, execute on schema::dbo to GenelkurmayBaskani
go
CREATE PROCEDURE TableFirst 
@TableName nvarchar(255), 
@ID int
AS 
BEGIN 
  SET NOCOUNT ON;
  DECLARE @Sql NVARCHAR(MAX);
SET @Sql = 'SELECT * FROM ' + QUOTENAME(@TableName) +' WHERE ID = ' + CAST(@ID as nvarchar)
 EXECUTE sp_executesql @Sql
END
GO
CREATE PROCEDURE TableSelect 
@TableName nvarchar(255)
AS 
BEGIN 
  SET NOCOUNT ON;
  DECLARE @Sql NVARCHAR(MAX);
SET @Sql = 'SELECT * FROM ' + QUOTENAME(@TableName)
 EXECUTE sp_executesql @Sql
END
go
CREATE PROC RanksUpdate
@ID int,
@Name nvarchar(255)
AS
UPDATE Ranks Set Name=@Name WHERE ID = @ID
go
CREATE PROC RanksInsert
@Name nvarchar(255)
AS
INSERT INTO Ranks(Name) VALUES(@Name)
go
CREATE PROC ArsenalUpdate
@ID int,
@Quantity int,
@Name nvarchar(255)
AS
UPDATE Arsenal Set Name=@Name, Quantity=@Quantity WHERE ID = @ID
go
CREATE PROC ArsenalInsert
@Name nvarchar(255),
@Quantity int
AS
INSERT INTO Arsenal(Name,Quantity) VALUES(@Name,@Quantity)
go
CREATE PROC VehicleCategoriesUpdate
@ID int,
@Name nvarchar(255)
AS
UPDATE VehicleCategories Set Name=@Name WHERE ID = @ID
go
CREATE PROC VehicleCategoriesInsert
@Name nvarchar(255)
AS
INSERT INTO VehicleCategories(Name) VALUES(@Name)
go
CREATE PROC VehiclesSelect
AS
SELECT Vehicles.ID as ID,Vehicles.Name as  VehicleName,usingUnit.Name as UsingUnitName,belongUnit.Name as BelongUnitName,VehicleCategories.Name as CategoryName FROM Vehicles 
LEFT JOIN VehicleCategories ON (VehicleCategories.ID = Vehicles.VehicleCategoryID)
LEFT JOIN Units belongUnit ON (belongUnit.ID = Vehicles.BelongingUnitID)
LEFT JOIN Units usingUnit ON (usingUnit.ID = Vehicles.UsingUnitID)
go
CREATE PROC VehiclesUpdate
@ID int,
@alternativeID int,
@Name nvarchar(255),
@VehicleCategoryID int,
@BelongingUnitID int,
@Status nvarchar(255),
@UsingUnitID int
AS
IF(@BelongingUnitID = 0) SET @BelongingUnitID = NULL;
IF(@UsingUnitID = 0) SET @UsingUnitID = NULL;
UPDATE Vehicles Set ID=@alternativeID, Name=@Name,
 VehicleCategoryID=@VehicleCategoryID,
  BelongingUnitID=@BelongingUnitID, 
  Status=@Status,
  UsingUnitID=@UsingUnitID
   WHERE ID = @ID
go
CREATE PROC VehiclesInsert
@ID int,
@Name nvarchar(255),
@VehicleCategoryID int,
@BelongingUnitID int,
@Status nvarchar(255),
@UsingUnitID int
AS
IF(@BelongingUnitID = 0) SET @BelongingUnitID = NULL;
IF(@UsingUnitID = 0) SET @UsingUnitID = NULL;
INSERT INTO Vehicles(ID,Name,VehicleCategoryID,BelongingUnitID,Status,UsingUnitID)
VALUES(@ID,@Name,@VehicleCategoryID,@BelongingUnitID,@Status,@UsingUnitID) 
GO
CREATE PROC WeaponCategoriesUpdate
@ID int,
@Name nvarchar(255)
AS
UPDATE WeaponCategories Set Name=@Name WHERE ID = @ID
go
CREATE PROC WeaponCategoriesInsert
@Name nvarchar(255)
AS
INSERT INTO WeaponCategories(Name) VALUES(@Name)
go
CREATE PROC WeaponsUpdate
@ID int,
@alternativeID int,
@Name nvarchar(255),
@WeaponCategoryID int,
@BelongingSoldierID int,
@Status nvarchar(255)
AS
IF(@BelongingSoldierID = 0) SET @BelongingSoldierID = NULL;
UPDATE Weapons Set ID=@alternativeID, Name=@Name,
 BelongingSoldierID=@BelongingSoldierID,
  WeaponCategoryID = @WeaponCategoryID,
  Status=@Status
   WHERE ID = @ID
go
CREATE PROC WeaponsInsert
@ID int,
@Name nvarchar(255),
@WeaponCategoryID int,
@BelongingSoldierID int,
@Status nvarchar(255)
AS
IF(@BelongingSoldierID = 0) SET @BelongingSoldierID = NULL;
INSERT INTO Weapons(ID,Name,WeaponCategoryID,BelongingSoldierID,Status)
VALUES(@ID,@Name,@WeaponCategoryID,@BelongingSoldierID,@Status)
go
CREATE PROC EquipmentsUpdate
@ID int,
@alternativeID int,
@Name nvarchar(255),
@BelongingSoldierID int,
@Status nvarchar(255)
AS
IF(@BelongingSoldierID = 0) SET @BelongingSoldierID = NULL;
UPDATE Equipments Set ID=@alternativeID,Name=@Name,
 BelongingSoldierID=@BelongingSoldierID,
  Status=@Status
   WHERE ID = @ID
go
CREATE PROC EquipmentsInsert
@ID int,
@Name nvarchar(255),
@BelongingSoldierID int,
@Status nvarchar(255)
AS
IF(@BelongingSoldierID = 0) SET @BelongingSoldierID = NULL;
INSERT INTO Equipments(ID,Name,BelongingSoldierID,Status)
VALUES(@ID,@Name,@BelongingSoldierID,@Status)
go
CREATE PROC EquipmentsSelect
AS
SELECT Equipments.ID,Name,FirstName,LastName,Equipments.Status FROM Equipments LEFT JOIN Soldiers ON (Soldiers.ID = Equipments.BelongingSoldierID)
go
CREATE PROC UnitTypesUpdate
@ID int,
@Name nvarchar(255)
AS
UPDATE UnitTypes Set Name=@Name WHERE ID = @ID
go
CREATE PROC UnitTypesInsert
@Name nvarchar(255)
AS
INSERT INTO UnitTypes(Name) VALUES(@Name)
GO
CREATE PROC UnitsSelect
AS
SELECT 
baseUnits.ID,
baseUnits.Name,
joinUnits.Name as parentUnitName,
UnitTypes.Name as UnitTypeName,
Soldiers.FirstName + ' ' + Soldiers.LastName as Leader
FROM
Units baseUnits
LEFT JOIN
Units joinUnits
ON (joinUnits.ID = baseUnits.ParentUnitID)
LEFT JOIN
UnitTypes 
ON (baseUnits.UnitTypeID = UnitTypes.ID)
LEFT JOIN
Soldiers
ON (Soldiers.ID = baseUnits.LeaderID)
GO	
CREATE PROC UnitsUpdate
@ID int,
@Name nvarchar(255),
@ParentUnitID int,
@UnitTypeID int,
@LeaderID int
AS
IF(@LeaderID = 0) SET @LeaderID = NULL;
IF(@ParentUnitID = 0) SET @ParentUnitID = NULL;
UPDATE Units Set 
Name=@Name,
ParentUnitID=@ParentUnitID,
UnitTypeID=@UnitTypeID,
LeaderID=@LeaderID
  WHERE ID = @ID
go
CREATE PROC UnitsInsert
@Name nvarchar(255),
@ParentUnitID int,
@UnitTypeID int,
@LeaderID int
AS
IF(@LeaderID = 0) SET @LeaderID = NULL;
IF(@ParentUnitID = 0) SET @ParentUnitID = NULL;
INSERT INTO Units(Name,ParentUnitID,UnitTypeID,LeaderID)
VALUES(@Name,@ParentUnitID,@UnitTypeID,@LeaderID) 
go
CREATE PROC DisciplinaryPunishmentsSelect
AS
SELECT DisciplinaryPunishments.ID,FirstName+' '+LastName as Name,Ranks.Name as Rank,
DisciplinaryPunishments.Description,
DisciplinaryPunishments.Date
 FROM DisciplinaryPunishments
LEFT JOIN Soldiers
ON (DisciplinaryPunishments.AppliedSoldierID = Soldiers.ID)
LEFT JOIN Ranks 
ON (Soldiers.RankID = Ranks.ID)
GO
CREATE PROC DisciplinaryPunishmentsUpdate
@ID int,
@AppliedSoldierID int,
@Date Date,
@Description nvarchar(500)
AS
UPDATE DisciplinaryPunishments Set Date=@Date,AppliedSoldierID=@AppliedSoldierID,Description=@Description WHERE ID = @ID
go
CREATE PROC DisciplinaryPunishmentsInsert
@AppliedSoldierID int,
@Date DateTime,
@Description nvarchar(500)
AS
INSERT INTO DisciplinaryPunishments(AppliedSoldierID,Date,Description)
VALUES(@AppliedSoldierID,@Date,@Description)
go
CREATE PROC SoldiersSelect
as
SELECT 
Soldiers.ID,
Soldiers.FirstName+' '+Soldiers.LastName as SoldierName,
Ranks.Name as RankName,
Units.Name as UnitName,
Soldiers.DateOfBirth,
Soldiers.StartingDate,
Soldiers.Status
 FROM Soldiers
LEFT JOIN Ranks
ON(Ranks.ID = Soldiers.RankID)
LEFT JOIN Units
ON (Units.ID = Soldiers.TeamID)
GO
CREATE PROC SoldiersInsert
@FirstName nvarchar(255),
@LastName nvarchar(255),
@DateOfBirth Date,
@StartingDate Date,
@RankID int,
@TeamID int,
@Status nvarchar(255)
AS
INSERT INTO Soldiers(FirstName,LastName,DateOfBirth,StartingDate,RankID,TeamID,Status)
VALUES(@FirstName,@LastName,@DateOfBirth,@StartingDate,@RankID,@TeamID,@Status)
go
CREATE PROC SoldiersUpdate
@ID int,
@FirstName nvarchar(255),
@LastName nvarchar(255),
@DateOfBirth Date,
@StartingDate Date,
@RankID int,
@TeamID int,
@Status nvarchar(255)
AS
UPDATE Soldiers SET FirstName=@FirstName,LastName=@LastName,DateOfBirth=@DateOfBirth,
StartingDate=@StartingDate, RankID=@RankID, TeamID=@TeamID, Status=@Status
WHERE ID = @ID
go
CREATE PROC OperationsUpdate
@ID int,
@ReportDirectory nvarchar(255),
@Time time,
@StartingDate DateTime,
@Name nvarchar(255)
AS
UPDATE Operations SET 
ReportDirectory  = @ReportDirectory,
Time = @Time,
StartingDate = @StartingDate,
Name = @Name
WHERE ID = @ID
go
CREATE PROC OperationsInsert
@ReportDirectory nvarchar(255),
@Time time,
@StartingDate DateTime,
@Name nvarchar(255)
AS
INSERT Operations(Name,Time,StartingDate,ReportDirectory)
VALUES(@Name,@Time,@StartingDate,@ReportDirectory)
go
CREATE PROC ArsenalUsagesByOperationID
@OperationID int
AS
SELECT 
ArsenalUsages.OperationID as OperationID,
Units.Name as UnitName,
Arsenal.Name as ArsenalName,
ArsenalUsages.Quantity as UsedQuantity,
Arsenal.ID as ArsenalID,
Units.ID as UnitID,
Operations.StartingDate as OperationStartingDate
FROM
ArsenalUsages
LEFT JOIN Units
ON (Units.ID = UsedUnitID)
LEFT JOIN Arsenal
ON (Arsenal.ID = UsedArsenalID)
LEFT JOIN Operations
ON (Operations.ID = OperationID)
WHERE OperationID = @OperationID
go
CREATE PROC removeArsenalUsagesByOperationID
@OperationID int
AS
DELETE FROM ArsenalUsages WHERE OperationID = @OperationID
go
CREATE PROC ArsenalUsagesInsert
@UsedUnitID int,
@UsedArsenalID int,
@Quantity int,
@OperationID int
AS
INSERT INTO ArsenalUsages(UsedUnitID,UsedArsenalID,Quantity,OperationID)
VALUES(@UsedUnitID,@UsedArsenalID,@Quantity,@OperationID)
GO
CREATE PROC ArsenalUsagesSelect
AS
SELECT Operations.Name as OperationsName,Arsenal.Name as ArsenalName,ArsenalUsages.Quantity as UsedArsenalQuantity,Units.Name as UnitName FROM ArsenalUsages
LEFT JOIN Operations
ON (Operations.ID = OperationID)
LEFT JOIN Arsenal
ON (Arsenal.ID = UsedArsenalID)
LEFT JOIN Units
ON(Units.ID = UsedUnitID)
GO
CREATE PROC SelectGroupByOperations
as
SELECT 
ArsenalUsages.OperationID,
Operations.Name
FROM ArsenalUsages
LEFT JOIN Operations
ON (Operations.ID = OperationID)
GROUP BY Operations.ID,ArsenalUsages.OperationID,Operations.Name
go
CREATE PROC removeUnitsInOperationByOperationID
@OperationID int
AS
DELETE FROM UnitsInOperations WHERE OperationID = @OperationID
go
CREATE PROC UnitsInOperationsInsert
@OperationID int,
@UnitID int
AS 
INSERT INTO UnitsInOperations(OperationID,UnitID)
VALUES(@OperationID,@UnitID)
go
CREATE PROC getLastRowFromTable
@TableName nvarchar(255)
AS 
BEGIN 
  SET NOCOUNT ON;
  DECLARE @Sql NVARCHAR(MAX);
SET @Sql = 'SELECT TOP 1 * FROM ' + QUOTENAME(@TableName) +'Operations ORDER BY ID DESC '
 EXECUTE sp_executesql @Sql
END
go
CREATE PROC getUnitsInOrdersByOperationID
@OperationID int
AS
SELECT Units.ID as UnitID,Units.Name as UnitName FROM UnitsInOperations
LEFT JOIN Units 
ON (Units.ID = UnitsInOperations.UnitID) WHERE UnitsInOperations.OperationID = @OperationID
go
CREATE PROC WeaponsSelect
AS
SELECT Weapons.ID, Weapons.Name,WeaponCategories.Name as CategoryName,Soldiers.FirstName+' '+Soldiers.LastName as SoldierName FROM Weapons
LEFT JOIN WeaponCategories
on(WeaponCategoryID = WeaponCategories.ID)
LEFT JOIN Soldiers
ON (Soldiers.ID = BelongingSoldierID)
go
CREATE PROC getOperationsByUnitID
@UnitID int
AS
SELECT Operations.Name FROM UnitsInOperations
LEFT JOIN Operations
ON(Operations.ID = UnitsInOperations.OperationID)
LEFT JOIN Units
ON(Units.ID = UnitsInOperations.UnitID)
WHERE UnitsInOperations.UnitID = @UnitID
go
CREATE PROC UnitsInOperationsSelect
AS
SELECT Operations.ID,Operations.Name as OperationName,Units.Name as UnitName FROM UnitsInOperations
LEFT JOIN Operations
ON(Operations.ID = UnitsInOperations.OperationID)
LEFT JOIN Units
ON(Units.ID = UnitsInOperations.UnitID)
go

