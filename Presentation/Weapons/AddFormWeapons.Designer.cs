﻿namespace Presentation.Weapons
{
    partial class AddFormWeapons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddFormWeapons));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.Label();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.weaponCategoryCombo = new System.Windows.Forms.ComboBox();
            this.belongingSoliderCombo = new System.Windows.Forms.ComboBox();
            this.doneButton = new System.Windows.Forms.Button();
            this.nameText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.idNumberTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Weapon Category";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Belonging Solider";
            // 
            // nameTextBox
            // 
            this.nameTextBox.AutoSize = true;
            this.nameTextBox.Location = new System.Drawing.Point(12, 121);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(37, 13);
            this.nameTextBox.TabIndex = 3;
            this.nameTextBox.Text = "Status";
            // 
            // statusTextBox
            // 
            this.statusTextBox.Location = new System.Drawing.Point(117, 118);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(186, 20);
            this.statusTextBox.TabIndex = 7;
            // 
            // weaponCategoryCombo
            // 
            this.weaponCategoryCombo.FormattingEnabled = true;
            this.weaponCategoryCombo.Location = new System.Drawing.Point(117, 64);
            this.weaponCategoryCombo.Name = "weaponCategoryCombo";
            this.weaponCategoryCombo.Size = new System.Drawing.Size(186, 21);
            this.weaponCategoryCombo.TabIndex = 8;
            // 
            // belongingSoliderCombo
            // 
            this.belongingSoliderCombo.FormattingEnabled = true;
            this.belongingSoliderCombo.Location = new System.Drawing.Point(117, 91);
            this.belongingSoliderCombo.Name = "belongingSoliderCombo";
            this.belongingSoliderCombo.Size = new System.Drawing.Size(186, 21);
            this.belongingSoliderCombo.TabIndex = 9;
            // 
            // doneButton
            // 
            this.doneButton.Location = new System.Drawing.Point(12, 144);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(291, 23);
            this.doneButton.TabIndex = 10;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = true;
            this.doneButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(117, 38);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(186, 20);
            this.nameText.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "ID";
            // 
            // idNumberTextBox
            // 
            this.idNumberTextBox.Location = new System.Drawing.Point(117, 12);
            this.idNumberTextBox.Name = "idNumberTextBox";
            this.idNumberTextBox.Size = new System.Drawing.Size(186, 20);
            this.idNumberTextBox.TabIndex = 13;
            // 
            // AddFormWeapons
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(315, 179);
            this.Controls.Add(this.idNumberTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.belongingSoliderCombo);
            this.Controls.Add(this.weaponCategoryCombo);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddFormWeapons";
            this.Text = "New Weapon";
            this.Load += new System.EventHandler(this.AddWeaponsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label nameTextBox;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.ComboBox weaponCategoryCombo;
        private System.Windows.Forms.ComboBox belongingSoliderCombo;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox idNumberTextBox;
    }
}