﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;
using DataLayer.Entity;
using DataLayer.Repositories;
namespace Presentation.Weapons
{
    public partial class AddFormWeapons : Form
    {
        int id;
        DataLayer.Entity.Weapons myWeapon;
        public AddFormWeapons(int id)
        {
            InitializeComponent();


            this.id = id;

            myWeapon = (DataLayer.Entity.Weapons)Facade.GetRecord(id, "Weapons");


            nameText.Text = myWeapon.Name;

            //weaponCategories
            weaponCategoryCombo.DataSource = Facade.GetDataAsDataTableByRepoName("WeaponCategories");
            weaponCategoryCombo.DisplayMember = "Name";
            weaponCategoryCombo.ValueMember = "ID";
            weaponCategoryCombo.SelectedValue = myWeapon.WeaponCategoryID;
            //end weaponCategories


            //belonging solider

            belongingSoliderCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Soldiers");
            belongingSoliderCombo.DisplayMember = "SoldierName";
            belongingSoliderCombo.ValueMember = "ID";
            belongingSoliderCombo.SelectedValue = myWeapon.BelongingSoldierID;
            //end belonging solider


            //status
            statusTextBox.Text = myWeapon.Status;
            //end status


            idNumberTextBox.Text = myWeapon.ID.ToString();


        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(Convert.ToInt32(idNumberTextBox.Text) == 0)
            {
                MessageBox.Show("ID CANNOT BE 0");
                return;
            }

            myWeapon.Name = nameText.Text;
            myWeapon.Status = statusTextBox.Text;
            myWeapon.BelongingSoldierID = belongingSoliderCombo.SelectedValue is null ? 0 : int.Parse(belongingSoliderCombo.SelectedValue.ToString());
            myWeapon.WeaponCategoryID = int.Parse(weaponCategoryCombo.SelectedValue.ToString());

            if (this.id == 0)
            {
                myWeapon.ID = int.Parse(idNumberTextBox.Text);
                Facade.NonAutoIncrementInsert(myWeapon, true);

            }
            else Facade.NonAutoIncrementUpdate(myWeapon, int.Parse(idNumberTextBox.Text));


            this.Close();

        }

        private void AddWeaponsForm_Load(object sender, EventArgs e)
        {

        }
    }
}
