﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;
using DataLayer;
namespace Presentation

{
    public static class FormManipulator
    {
        public static List<Form> activeFormList = new List<Form>();


        #region SHOW FORM
        public static void ShowForm(Form form, Form mdiParentForm, ListingForm myListingForm = null)
        {


            Form ff = activeFormList.FirstOrDefault(x => x.Name == form.Name);



            if (ff == null)
            {
                activeFormList.Add(form);
                form.FormClosing += Form_FormClosing;
                if (myListingForm != null) form.FormClosing += myListingForm.reloadDataGridVieFormClosingEvent;
                if(!(form is ListingForm))
                {
                    form.FormBorderStyle = FormBorderStyle.FixedDialog;
                    form.MaximizeBox = false;
                }
                form.MdiParent = mdiParentForm;
                form.Show();
            }
            else
            {
                Notification.alert("The form you have requested is already open.");
            }





        }

        private static void Form_FormClosing1(object sender, FormClosingEventArgs e)
        {

        }

        private static void Form_FormClosing(object sender, FormClosingEventArgs e)
        {

            activeFormList.Remove((Form)sender);
        }


        #endregion SHOW FORM
    }
}
