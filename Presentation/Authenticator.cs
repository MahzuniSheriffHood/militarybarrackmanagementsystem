﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class Authenticator : Form
    {
        public Authenticator()
        {
            InitializeComponent();
        }
        //semih
        private void Authenticator_Load(object sender, EventArgs e)
        {
            textBox1.Text = "HulusiAkar";
            textBox2.Text = "123456";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Facade.Authenticate(textBox1.Text, textBox2.Text))
            {
                MainForm mf = new MainForm(textBox1.Text);
                mf.FormClosed += Authenticator_FormClosed;
                this.Hide();
                mf.ShowDialog();
            }
            else
            {
                MessageBox.Show("The username or password you entered is incorrect");
            }
        }

        private void Authenticator_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
