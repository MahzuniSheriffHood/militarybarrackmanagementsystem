﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.UnitTypes
{
    public partial class AddFormUnitTypes : Form
    {

        DataLayer.Entity.UnitTypes myUnitType;
        int id;
        public AddFormUnitTypes(int id)
        {
            InitializeComponent();


            this.id = id;
            myUnitType = (DataLayer.Entity.UnitTypes)Facade.GetRecord(id, "UnitTypes");
            nameTextBox.Text = myUnitType.Name;
        }

        private void button1_Click(object sender, EventArgs e)
        {


            myUnitType.Name = nameTextBox.Text;
            Facade.HandleAddForm(myUnitType);

            this.Close();

        }
    }
}
