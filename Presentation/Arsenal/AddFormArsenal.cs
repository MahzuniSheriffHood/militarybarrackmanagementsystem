﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;
namespace Presentation.Arsenal
{
    public partial class AddFormArsenal : Form
    {


        DataLayer.Entity.Arsenal myArsenal;
        int id;
        public AddFormArsenal(int id)
        {
            InitializeComponent();



            this.id = id;
            this.myArsenal = (DataLayer.Entity.Arsenal)Facade.GetRecord(id, "Arsenal");
            nameTexBox.Text = myArsenal.Name;
            quantityTextBox.Text = myArsenal.Quantity.ToString();

        }

        private void ArsenalAddForm_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            myArsenal.Name = nameTexBox.Text;
            myArsenal.Quantity = int.Parse(quantityTextBox.Text);

            Facade.HandleAddForm(myArsenal);


            this.Close();
            //UpdateOrInsert<DataLayer.Repositories.Arsenal>

        }
    }
}
