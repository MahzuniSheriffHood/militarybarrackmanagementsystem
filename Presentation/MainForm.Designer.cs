﻿namespace Presentation
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.RanksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arsenalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.vehiclesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.weaponsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.EquipmentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportByStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ReportByBelongingSoldierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitTypesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.diciplineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.soldiersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.operationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.unitsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.arsenalUsagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RanksToolStripMenuItem,
            this.arsenalToolStripMenuItem,
            this.vehiclesToolStripMenuItem,
            this.weaponsToolStripMenuItem,
            this.EquipmentsToolStripMenuItem,
            this.unitsToolStripMenuItem,
            this.diciplineToolStripMenuItem,
            this.soldiersToolStripMenuItem,
            this.operationsToolStripMenuItem,
            this.arsenalUsagesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(841, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // RanksToolStripMenuItem
            // 
            this.RanksToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem,
            this.ReportToolStripMenuItem,
            this.newToolStripMenuItem});
            this.RanksToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("RanksToolStripMenuItem.Image")));
            this.RanksToolStripMenuItem.Name = "RanksToolStripMenuItem";
            this.RanksToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.RanksToolStripMenuItem.Text = "Ranks";
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem.Tag = "Ranks";
            this.listToolStripMenuItem.Text = "List";
            this.listToolStripMenuItem.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // ReportToolStripMenuItem
            // 
            this.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem";
            this.ReportToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ReportToolStripMenuItem.Tag = "Ranks-Report";
            this.ReportToolStripMenuItem.Text = "Report";
            this.ReportToolStripMenuItem.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // arsenalToolStripMenuItem
            // 
            this.arsenalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem1,
            this.reportToolStripMenuItem1,
            this.newToolStripMenuItem1});
            this.arsenalToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("arsenalToolStripMenuItem.Image")));
            this.arsenalToolStripMenuItem.Name = "arsenalToolStripMenuItem";
            this.arsenalToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.arsenalToolStripMenuItem.Text = "Arsenal";
            // 
            // listToolStripMenuItem1
            // 
            this.listToolStripMenuItem1.Name = "listToolStripMenuItem1";
            this.listToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem1.Tag = "Arsenal";
            this.listToolStripMenuItem1.Text = "List";
            this.listToolStripMenuItem1.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem1
            // 
            this.reportToolStripMenuItem1.Name = "reportToolStripMenuItem1";
            this.reportToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem1.Tag = "Arsenal-Report";
            this.reportToolStripMenuItem1.Text = "Report";
            this.reportToolStripMenuItem1.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // newToolStripMenuItem1
            // 
            this.newToolStripMenuItem1.Name = "newToolStripMenuItem1";
            this.newToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem1.Text = "New";
            this.newToolStripMenuItem1.Click += new System.EventHandler(this.newToolStripMenuItem1_Click);
            // 
            // vehiclesToolStripMenuItem
            // 
            this.vehiclesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoriesToolStripMenuItem,
            this.listToolStripMenuItem3,
            this.newToolStripMenuItem3,
            this.reportToolStripMenuItem7});
            this.vehiclesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("vehiclesToolStripMenuItem.Image")));
            this.vehiclesToolStripMenuItem.Name = "vehiclesToolStripMenuItem";
            this.vehiclesToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.vehiclesToolStripMenuItem.Text = "Vehicles";
            // 
            // categoriesToolStripMenuItem
            // 
            this.categoriesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem2,
            this.newToolStripMenuItem2,
            this.reportToolStripMenuItem3});
            this.categoriesToolStripMenuItem.Name = "categoriesToolStripMenuItem";
            this.categoriesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.categoriesToolStripMenuItem.Text = "Categories";
            // 
            // listToolStripMenuItem2
            // 
            this.listToolStripMenuItem2.Name = "listToolStripMenuItem2";
            this.listToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem2.Tag = "Vehicle Categories";
            this.listToolStripMenuItem2.Text = "List";
            this.listToolStripMenuItem2.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem2
            // 
            this.newToolStripMenuItem2.Name = "newToolStripMenuItem2";
            this.newToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem2.Text = "New";
            this.newToolStripMenuItem2.Click += new System.EventHandler(this.newToolStripMenuItem2_Click);
            // 
            // reportToolStripMenuItem3
            // 
            this.reportToolStripMenuItem3.Name = "reportToolStripMenuItem3";
            this.reportToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem3.Tag = "VehicleCategories-Report";
            this.reportToolStripMenuItem3.Text = "Report";
            this.reportToolStripMenuItem3.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // listToolStripMenuItem3
            // 
            this.listToolStripMenuItem3.Name = "listToolStripMenuItem3";
            this.listToolStripMenuItem3.Size = new System.Drawing.Size(176, 22);
            this.listToolStripMenuItem3.Tag = "Vehicles";
            this.listToolStripMenuItem3.Text = "List";
            this.listToolStripMenuItem3.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem3
            // 
            this.newToolStripMenuItem3.Name = "newToolStripMenuItem3";
            this.newToolStripMenuItem3.Size = new System.Drawing.Size(176, 22);
            this.newToolStripMenuItem3.Text = "New";
            this.newToolStripMenuItem3.Click += new System.EventHandler(this.newToolStripMenuItem3_Click);
            // 
            // reportToolStripMenuItem7
            // 
            this.reportToolStripMenuItem7.Name = "reportToolStripMenuItem7";
            this.reportToolStripMenuItem7.Size = new System.Drawing.Size(176, 22);
            this.reportToolStripMenuItem7.Tag = "Vehicles-ReportByUnitName";
            this.reportToolStripMenuItem7.Text = "ReportByUnitName";
            this.reportToolStripMenuItem7.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // weaponsToolStripMenuItem
            // 
            this.weaponsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.categoriesToolStripMenuItem1,
            this.listToolStripMenuItem5,
            this.newToolStripMenuItem5,
            this.reportToolStripMenuItem12});
            this.weaponsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("weaponsToolStripMenuItem.Image")));
            this.weaponsToolStripMenuItem.Name = "weaponsToolStripMenuItem";
            this.weaponsToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.weaponsToolStripMenuItem.Text = "Weapons";
            // 
            // categoriesToolStripMenuItem1
            // 
            this.categoriesToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem4,
            this.newToolStripMenuItem4,
            this.reportToolStripMenuItem2});
            this.categoriesToolStripMenuItem1.Name = "categoriesToolStripMenuItem1";
            this.categoriesToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.categoriesToolStripMenuItem1.Text = "Categories";
            // 
            // listToolStripMenuItem4
            // 
            this.listToolStripMenuItem4.Name = "listToolStripMenuItem4";
            this.listToolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem4.Tag = "Weapon Categories";
            this.listToolStripMenuItem4.Text = "List";
            this.listToolStripMenuItem4.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem4
            // 
            this.newToolStripMenuItem4.Name = "newToolStripMenuItem4";
            this.newToolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem4.Text = "New";
            this.newToolStripMenuItem4.Click += new System.EventHandler(this.newToolStripMenuItem4_Click_1);
            // 
            // reportToolStripMenuItem2
            // 
            this.reportToolStripMenuItem2.Name = "reportToolStripMenuItem2";
            this.reportToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem2.Tag = "WeaponCategories-Report";
            this.reportToolStripMenuItem2.Text = "Report";
            this.reportToolStripMenuItem2.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // listToolStripMenuItem5
            // 
            this.listToolStripMenuItem5.Name = "listToolStripMenuItem5";
            this.listToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem5.Tag = "Weapons";
            this.listToolStripMenuItem5.Text = "List";
            this.listToolStripMenuItem5.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem5
            // 
            this.newToolStripMenuItem5.Name = "newToolStripMenuItem5";
            this.newToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem5.Text = "New";
            this.newToolStripMenuItem5.Click += new System.EventHandler(this.newToolStripMenuItem5_Click);
            // 
            // reportToolStripMenuItem12
            // 
            this.reportToolStripMenuItem12.Name = "reportToolStripMenuItem12";
            this.reportToolStripMenuItem12.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem12.Tag = "Weapons-Report";
            this.reportToolStripMenuItem12.Text = "Report";
            this.reportToolStripMenuItem12.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // EquipmentsToolStripMenuItem
            // 
            this.EquipmentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem6,
            this.addToolStripMenuItem,
            this.ReportByStatusToolStripMenuItem,
            this.ReportByBelongingSoldierToolStripMenuItem});
            this.EquipmentsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("EquipmentsToolStripMenuItem.Image")));
            this.EquipmentsToolStripMenuItem.Name = "EquipmentsToolStripMenuItem";
            this.EquipmentsToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.EquipmentsToolStripMenuItem.Text = "Equipments";
            // 
            // listToolStripMenuItem6
            // 
            this.listToolStripMenuItem6.Name = "listToolStripMenuItem6";
            this.listToolStripMenuItem6.Size = new System.Drawing.Size(212, 22);
            this.listToolStripMenuItem6.Tag = "Equipments";
            this.listToolStripMenuItem6.Text = "List";
            this.listToolStripMenuItem6.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // ReportByStatusToolStripMenuItem
            // 
            this.ReportByStatusToolStripMenuItem.Name = "ReportByStatusToolStripMenuItem";
            this.ReportByStatusToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ReportByStatusToolStripMenuItem.Tag = "Equipments-ReportByStatus";
            this.ReportByStatusToolStripMenuItem.Text = "ReportByStatus";
            this.ReportByStatusToolStripMenuItem.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // ReportByBelongingSoldierToolStripMenuItem
            // 
            this.ReportByBelongingSoldierToolStripMenuItem.Name = "ReportByBelongingSoldierToolStripMenuItem";
            this.ReportByBelongingSoldierToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.ReportByBelongingSoldierToolStripMenuItem.Tag = "Equipments-ReportByBelongingSoldier";
            this.ReportByBelongingSoldierToolStripMenuItem.Text = "ReportByBelongingSoldier";
            this.ReportByBelongingSoldierToolStripMenuItem.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // unitsToolStripMenuItem
            // 
            this.unitsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unitTypesToolStripMenuItem,
            this.listToolStripMenuItem8,
            this.addToolStripMenuItem1,
            this.reportToolStripMenuItem8});
            this.unitsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("unitsToolStripMenuItem.Image")));
            this.unitsToolStripMenuItem.Name = "unitsToolStripMenuItem";
            this.unitsToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.unitsToolStripMenuItem.Text = "Units";
            // 
            // unitTypesToolStripMenuItem
            // 
            this.unitTypesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem7,
            this.reportToolStripMenuItem5});
            this.unitTypesToolStripMenuItem.Name = "unitTypesToolStripMenuItem";
            this.unitTypesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.unitTypesToolStripMenuItem.Text = "UnitTypes";
            // 
            // listToolStripMenuItem7
            // 
            this.listToolStripMenuItem7.Name = "listToolStripMenuItem7";
            this.listToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem7.Tag = "Unit Types";
            this.listToolStripMenuItem7.Text = "List";
            this.listToolStripMenuItem7.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem5
            // 
            this.reportToolStripMenuItem5.Name = "reportToolStripMenuItem5";
            this.reportToolStripMenuItem5.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem5.Tag = "UnitTypes-Report";
            this.reportToolStripMenuItem5.Text = "Report";
            this.reportToolStripMenuItem5.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // listToolStripMenuItem8
            // 
            this.listToolStripMenuItem8.Name = "listToolStripMenuItem8";
            this.listToolStripMenuItem8.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem8.Tag = "Units";
            this.listToolStripMenuItem8.Text = "List";
            this.listToolStripMenuItem8.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem1
            // 
            this.addToolStripMenuItem1.Name = "addToolStripMenuItem1";
            this.addToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.addToolStripMenuItem1.Text = "Add";
            this.addToolStripMenuItem1.Click += new System.EventHandler(this.addToolStripMenuItem1_Click);
            // 
            // reportToolStripMenuItem8
            // 
            this.reportToolStripMenuItem8.Name = "reportToolStripMenuItem8";
            this.reportToolStripMenuItem8.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem8.Tag = "Units-Report";
            this.reportToolStripMenuItem8.Text = "Report";
            this.reportToolStripMenuItem8.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // diciplineToolStripMenuItem
            // 
            this.diciplineToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem9,
            this.newToolStripMenuItem6,
            this.reportToolStripMenuItem9});
            this.diciplineToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("diciplineToolStripMenuItem.Image")));
            this.diciplineToolStripMenuItem.Name = "diciplineToolStripMenuItem";
            this.diciplineToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.diciplineToolStripMenuItem.Text = "Dicipline";
            // 
            // listToolStripMenuItem9
            // 
            this.listToolStripMenuItem9.Name = "listToolStripMenuItem9";
            this.listToolStripMenuItem9.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem9.Tag = "Disciplinary Punishments";
            this.listToolStripMenuItem9.Text = "List";
            this.listToolStripMenuItem9.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem6
            // 
            this.newToolStripMenuItem6.Name = "newToolStripMenuItem6";
            this.newToolStripMenuItem6.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem6.Text = "New";
            this.newToolStripMenuItem6.Click += new System.EventHandler(this.newToolStripMenuItem6_Click);
            // 
            // reportToolStripMenuItem9
            // 
            this.reportToolStripMenuItem9.Name = "reportToolStripMenuItem9";
            this.reportToolStripMenuItem9.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem9.Tag = "DisciplinaryPunishments-Report";
            this.reportToolStripMenuItem9.Text = "Report";
            this.reportToolStripMenuItem9.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // soldiersToolStripMenuItem
            // 
            this.soldiersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem10,
            this.newToolStripMenuItem7,
            this.reportToolStripMenuItem6});
            this.soldiersToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("soldiersToolStripMenuItem.Image")));
            this.soldiersToolStripMenuItem.Name = "soldiersToolStripMenuItem";
            this.soldiersToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.soldiersToolStripMenuItem.Text = "Soldiers";
            // 
            // listToolStripMenuItem10
            // 
            this.listToolStripMenuItem10.Name = "listToolStripMenuItem10";
            this.listToolStripMenuItem10.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem10.Tag = "Soldiers";
            this.listToolStripMenuItem10.Text = "List";
            this.listToolStripMenuItem10.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem7
            // 
            this.newToolStripMenuItem7.Name = "newToolStripMenuItem7";
            this.newToolStripMenuItem7.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem7.Text = "New";
            this.newToolStripMenuItem7.Click += new System.EventHandler(this.newToolStripMenuItem7_Click);
            // 
            // reportToolStripMenuItem6
            // 
            this.reportToolStripMenuItem6.Name = "reportToolStripMenuItem6";
            this.reportToolStripMenuItem6.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem6.Tag = "Soldiers-Report";
            this.reportToolStripMenuItem6.Text = "Report";
            this.reportToolStripMenuItem6.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // operationsToolStripMenuItem
            // 
            this.operationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem11,
            this.addToolStripMenuItem3,
            this.reportToolStripMenuItem4,
            this.unitsToolStripMenuItem1});
            this.operationsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("operationsToolStripMenuItem.Image")));
            this.operationsToolStripMenuItem.Name = "operationsToolStripMenuItem";
            this.operationsToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.operationsToolStripMenuItem.Text = "Operations";
            // 
            // listToolStripMenuItem11
            // 
            this.listToolStripMenuItem11.Name = "listToolStripMenuItem11";
            this.listToolStripMenuItem11.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem11.Tag = "Operations";
            this.listToolStripMenuItem11.Text = "List";
            this.listToolStripMenuItem11.Click += new System.EventHandler(this.listToolStripMenuItem_Click);
            // 
            // addToolStripMenuItem3
            // 
            this.addToolStripMenuItem3.Name = "addToolStripMenuItem3";
            this.addToolStripMenuItem3.Size = new System.Drawing.Size(152, 22);
            this.addToolStripMenuItem3.Text = "Add";
            this.addToolStripMenuItem3.Click += new System.EventHandler(this.addToolStripMenuItem3_Click);
            // 
            // reportToolStripMenuItem4
            // 
            this.reportToolStripMenuItem4.Name = "reportToolStripMenuItem4";
            this.reportToolStripMenuItem4.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem4.Tag = "Operations-Report";
            this.reportToolStripMenuItem4.Text = "Report";
            this.reportToolStripMenuItem4.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // unitsToolStripMenuItem1
            // 
            this.unitsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportToolStripMenuItem11});
            this.unitsToolStripMenuItem1.Name = "unitsToolStripMenuItem1";
            this.unitsToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.unitsToolStripMenuItem1.Text = "Units";
            // 
            // reportToolStripMenuItem11
            // 
            this.reportToolStripMenuItem11.Name = "reportToolStripMenuItem11";
            this.reportToolStripMenuItem11.Size = new System.Drawing.Size(109, 22);
            this.reportToolStripMenuItem11.Tag = "UnitsInOperations-Report";
            this.reportToolStripMenuItem11.Text = "Report";
            this.reportToolStripMenuItem11.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // arsenalUsagesToolStripMenuItem
            // 
            this.arsenalUsagesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listToolStripMenuItem12,
            this.addToolStripMenuItem2,
            this.reportToolStripMenuItem10});
            this.arsenalUsagesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("arsenalUsagesToolStripMenuItem.Image")));
            this.arsenalUsagesToolStripMenuItem.Name = "arsenalUsagesToolStripMenuItem";
            this.arsenalUsagesToolStripMenuItem.Size = new System.Drawing.Size(114, 20);
            this.arsenalUsagesToolStripMenuItem.Text = "Arsenal Usages";
            // 
            // listToolStripMenuItem12
            // 
            this.listToolStripMenuItem12.Name = "listToolStripMenuItem12";
            this.listToolStripMenuItem12.Size = new System.Drawing.Size(152, 22);
            this.listToolStripMenuItem12.Tag = "Arsenal Usages";
            this.listToolStripMenuItem12.Text = "List";
            this.listToolStripMenuItem12.Click += new System.EventHandler(this.listToolStripMenuItem12_Click);
            // 
            // addToolStripMenuItem2
            // 
            this.addToolStripMenuItem2.Name = "addToolStripMenuItem2";
            this.addToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.addToolStripMenuItem2.Text = "Add";
            this.addToolStripMenuItem2.Click += new System.EventHandler(this.addToolStripMenuItem2_Click);
            // 
            // reportToolStripMenuItem10
            // 
            this.reportToolStripMenuItem10.Name = "reportToolStripMenuItem10";
            this.reportToolStripMenuItem10.Size = new System.Drawing.Size(152, 22);
            this.reportToolStripMenuItem10.Tag = "ArsenalUsages-Report";
            this.reportToolStripMenuItem10.Text = "Report";
            this.reportToolStripMenuItem10.Click += new System.EventHandler(this.ReportItemClicks);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 345);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem RanksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arsenalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem vehiclesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem weaponsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem EquipmentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitTypesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ReportByStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ReportByBelongingSoldierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem diciplineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem soldiersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem operationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem arsenalUsagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem unitsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem12;
    }
}

