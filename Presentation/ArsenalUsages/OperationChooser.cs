﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.ArsenalUsages
{
    public partial class OperationChooser : Form
    {
        public OperationChooser()
        {
            InitializeComponent();
        }

        private void OperationChooser_Load(object sender, EventArgs e)
        {
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Operations");
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "ID";
            comboBox1.SelectedIndexChanged += comboBox1_SelectedIndexChanged;
            comboBox1.KeyUp += ComboBox1_KeyUp;
        }

        private void ComboBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                FormManipulator.ShowForm(new AddFormArsenalUsages(0, comboBox1.SelectedIndex + 1), this.MdiParent);
                this.Close();
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormArsenalUsages(0, comboBox1.SelectedIndex + 1), this.MdiParent);
            this.Close();
        }
    }
}
