﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.ArsenalUsages
{
    public partial class AddFormArsenalUsages : Form
    {
        int id;
        int operationID;

        private void baseCtor(int id)
        {
            this.id = id;
            DataTable myUsages = Business.Facade.GetDataAsDataTableByRepoName("ArsenalUsages", "ArsenalUsagesByOprationID", new object[] { id });
            foreach (DataRow x in myUsages.Rows)
            {

                dataGridView1.Rows.Add(new string[] {

                    x["UnitName"].ToString(),
                      x["ArsenalName"].ToString(),
                            x["UsedQuantity"].ToString(),
                            x["ArsenalID"].ToString(),
                          x["UnitID"].ToString(),



                });

            }


            //operations
            operationCombo.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Operations");
            operationCombo.DisplayMember = "Name";
            operationCombo.ValueMember = "ID";
            operationCombo.SelectedValue = operationID;
            //end operations

            //units
            unitCombo.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Units");
            unitCombo.DisplayMember = "Name";
            unitCombo.ValueMember = "ID";
            //end units

            //arsenals
            ArsenalCombo.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Arsenal");
            ArsenalCombo.DisplayMember = "Name";
            ArsenalCombo.ValueMember = "ID";
            //end arsenals

            dataGridView1.Columns["ArsenalID"].Visible = false;
            dataGridView1.Columns["UnitID"].Visible = false;
            // dataGridView1.AllowUserToAddRows = true;
            dataGridView1.AutoGenerateColumns = false;
        }

        public AddFormArsenalUsages(int id)
        {
            InitializeComponent();
            operationID = id;
            baseCtor(id);
        }

        public AddFormArsenalUsages(int id, int operationid)
        {
            InitializeComponent();
            operationID = operationid;
            baseCtor(id);
        }

        private void AddArsenalUsagesForm_Load(object sender, EventArgs e)
        {

        }

        bool checkHasArsenal(DataGridView dg, string unitID, string arsenalID)
        {


            int i = 0;
            foreach (DataGridViewRow row in dg.Rows)
            {

                //Console.WriteLine("UnitID" + row.Cells["UnitID"].Value.ToString());
                if (row.Cells[3].Value.ToString() == arsenalID && row.Cells[4].Value.ToString() == unitID)
                {
                    //&& row.Cells[4].Value.ToString() == unitID
                    int oldValue = int.Parse(dataGridView1.Rows[i].Cells[2].Value.ToString());

                    Console.WriteLine("Old Quantity ->" + oldValue.ToString());

                    int newValue = oldValue + int.Parse(quantityText.Text);

                    Console.WriteLine("New Quantity ->" + newValue.ToString());
                    dataGridView1.Rows[i].Cells[2].Value = newValue;
                    return true;

                }

                i++;
            }






            return false;


        }

        private void button2_Click(object sender, EventArgs e)
        {

            // Console.WriteLine(dataGridView1.Rows[0].Cells["UnitID"].Value);

            bool index = checkHasArsenal(dataGridView1, unitCombo.SelectedValue.ToString(), ArsenalCombo.SelectedValue.ToString());

            if (index) Console.WriteLine("VAR");
            else if (!index)
            {
                dataGridView1.Rows.Add(new string[] {

                    unitCombo.Text.ToString(),
                    ArsenalCombo.Text.ToString(),
                     quantityText.Text,
                    ArsenalCombo.SelectedValue.ToString(),
                    unitCombo.SelectedValue.ToString()



                });
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {



            Business.Facade.RemoveArsenalUsageByOperationID(this.id);


            foreach (DataGridViewRow row in dataGridView1.Rows)
            {

                DataLayer.Entity.ArsenalUsages myUsage = new DataLayer.Entity.ArsenalUsages();
                myUsage.OperationID = int.Parse(operationCombo.SelectedValue.ToString());
                myUsage.UsedArsenalID = int.Parse(row.Cells[3].Value.ToString());
                myUsage.UseDUnitID = int.Parse(row.Cells[4].Value.ToString());
                myUsage.Quantity = int.Parse(row.Cells[2].Value.ToString());
                try
                {
                    Business.Facade.HandleAddForm(myUsage);
                }
                catch (Exception)
                {

                    Notification.alert("Kayıt Eklenemiyor");
                }
            }







        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            dataGridView1.Rows.RemoveAt(e.RowIndex);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            unitCombo.SelectedValue = dataGridView1.Rows[e.RowIndex].Cells[4].Value;
            ArsenalCombo.SelectedValue = dataGridView1.Rows[e.RowIndex].Cells[3].Value;
            quantityText.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
