﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLayer;
using DataLayer.Entity;
namespace Presentation.Equipments
{


    
    
    public partial class AddFormEquipments : Form
    {

        DataLayer.Entity.Equipments myEquipments;
        int id;
        public AddFormEquipments(int id)
        {
            InitializeComponent();

            this.id = id;
            myEquipments = (DataLayer.Entity.Equipments)Business.Facade.GetRecord(id, "Equipments");




            nameTextBox.Text = myEquipments.Name;
            //belonging solider
            belongingSoliderCombo.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Soldiers");
            belongingSoliderCombo.DisplayMember = "SoldierName";
            belongingSoliderCombo.ValueMember = "ID";
            belongingSoliderCombo.SelectedValue = myEquipments.BelongingSoldierID;
            //end belonging solider

            statusTextBox.Text = myEquipments.Status;
            idNumberTextBox.Text = myEquipments.ID.ToString();


        }

        private void AddEquipmentsForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(idNumberTextBox.Text) == 0)
            {
                MessageBox.Show("ID CANNOT BE 0");
                return;
            }

            myEquipments.Name = nameTextBox.Text;
            myEquipments.BelongingSoldierID = (belongingSoliderCombo.SelectedValue is null) ? 0 : int.Parse(belongingSoliderCombo.SelectedValue.ToString());
            myEquipments.Status = statusTextBox.Text;


            if (this.id == 0)
            {
                myEquipments.ID = int.Parse(idNumberTextBox.Text);
                Business.Facade.NonAutoIncrementInsert(myEquipments, true);

            }
            else Business.Facade.NonAutoIncrementUpdate(myEquipments, int.Parse(idNumberTextBox.Text));


            this.Close();


        }
    }
}
