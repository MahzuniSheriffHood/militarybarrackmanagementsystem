﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using System.Reflection;

namespace Business
{
    public static class UpdateOrInsert  <T>
    {



        public static void UpdateOrInsertChooser(T myEntity)
        {

            int ID = (int)myEntity.GetType().GetProperty("ID").GetValue(myEntity, null);

            if (ID != 0 && ID != null)
            {
                //update




                object myFacade = Activator.CreateInstance(Type.GetType("DataLayer.Facade." + myEntity.GetType().Name));
                
                  myFacade.GetType().GetMethod("Update").Invoke(null, new[] { (object)myEntity });

            }
            else
        
            {

                //insert

                object myFacade = Activator.CreateInstance(Type.GetType("DataLayer.Facade." + myEntity.GetType().Name));
                
                  myFacade.GetType().GetMethod("Insert").Invoke(null, new[] { (object)myEntity });

            }


        }


    }
}
