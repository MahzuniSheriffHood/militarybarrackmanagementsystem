﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class ListingForm : Form
    {
        string repoName;
        string alternativeStoreProcName = null;
        public ListingForm(string repoName, string title = null, Image img = null, string alternativeStoreProcName = null)
        {
            InitializeComponent();

            this.repoName = repoName;
            this.Name = repoName;
            this.alternativeStoreProcName = alternativeStoreProcName;
            reloadDataGridView();
            Bitmap b = (Bitmap)img;
            IntPtr pIcon = b.GetHicon();
            Icon i = Icon.FromHandle(pIcon);
            Icon = i;
            i.Dispose();
            Text = title;
        }
        public void reloadDataGridView()
        {
            dataGridView1.DataSource = Business.Facade.GetDataAsDataTableByRepoName(this.repoName, this.alternativeStoreProcName);
        }

        public void reloadDataGridVieFormClosingEvent(object sender, FormClosingEventArgs e)
        {

            reloadDataGridView();
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void ListingForm_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int id = int.Parse(dataGridView1[0, e.RowIndex].Value.ToString());
            object myForm = Activator.CreateInstance(Type.GetType("Presentation." + this.repoName + ".AddForm" + this.repoName), new object[] { id });
            FormManipulator.ShowForm((Form)myForm, this.MdiParent, this);
        }

        private void reloadData_Click(object sender, EventArgs e)
        {
            reloadDataGridView();
        }
    }
}
