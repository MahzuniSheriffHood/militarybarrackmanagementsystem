﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;
using DataLayer;
namespace Presentation.Ranks
{
    public partial class AddFormRanks : Form
    {

        int id;
        DataLayer.Entity.Ranks myRank;
    
        public AddFormRanks(int id)
        {
            InitializeComponent();

            this.id = id;
            myRank = (DataLayer.Entity.Ranks)Facade.GetRecord(id, "Ranks");
            ranksName.Text = myRank.Name;
      
            
        }
     

        private void RanksAddForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            Console.WriteLine(myRank.ID);
            myRank.Name = ranksName.Text;
            Facade.HandleAddForm(myRank);
          
            this.Close();
            

        }
    }
}
