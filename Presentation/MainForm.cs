﻿using System;
using System.Windows.Forms;
using Presentation.Ranks;
using Presentation.Arsenal;
using Presentation.VehicleCategories;
using Presentation.Vehicles;
using Presentation.WeaponCategories;
using Presentation.Weapons;
using Presentation.Equipments;
using Presentation.UnitTypes;
using Presentation.Units;
using Presentation.DisciplinaryPunishments;
using Presentation.Soldiers;
using System.Reflection;
using System.Data;
using Presentation.ArsenalUsages;
using Presentation.Operations;
namespace Presentation
{
    public partial class MainForm : Form
    {
        public MainForm(string Username)
        {
            InitializeComponent();
            Text = "Welcome " + Username;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;

        }

        ToolStripItem GetMainStrip(ToolStripMenuItem item)
        {
            if(item == null)
            {
                return null;
            }
            ToolStripItem tsmi = item.OwnerItem;
            while(tsmi.OwnerItem != null)
            {
                tsmi = tsmi.OwnerItem;
            }
            return tsmi;
        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripItem tsmi = sender as ToolStripItem;
            ToolStripItem owner = GetMainStrip(sender as ToolStripMenuItem);
            FormManipulator.ShowForm(new ListingForm(tsmi.Tag.ToString().Replace(" ", null), tsmi.Tag.ToString(), owner.Image),this, null);


        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {


            FormManipulator.ShowForm(new AddFormRanks(0), this, null);

        }


        private void newToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormArsenal(0), this, null);
        }


        private void newToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormVehicleCategories(0), this, null);
        }


        private void newToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormVehicles(0), this, null);
        }


        private void newToolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }


        private void newToolStripMenuItem4_Click_1(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormWeaponCategories(0), this, null);
        }

        private void newToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormWeapons(0), this, null);
        }


        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormEquipments(0), this, null);
        }

        
        private void ReportItemClicks(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = sender as ToolStripMenuItem;
            new ReportTemplate(tsmi.Tag.ToString()) { MdiParent = this }.Show();
        }

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormUnits(0), this, null);
        }


        private void newToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormDisciplinaryPunishments(0), this, null);
        }


        private void newToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormSoldiers(0), this, null);
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void addToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new OperationChooser(), this, null);
        }

        private void addToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            FormManipulator.ShowForm(new AddFormOperations(0), this, null);
        }

        private void listToolStripMenuItem12_Click(object sender, EventArgs e)
        {
            ToolStripItem tsmi = sender as ToolStripItem;
            ToolStripItem owner = GetMainStrip(sender as ToolStripMenuItem);
            FormManipulator.ShowForm(new ListingForm(tsmi.Tag.ToString().Replace(" ", null), tsmi.Tag.ToString(), owner.Image, "SelectGroupByOperations"), this, null);

        }
    }
}
