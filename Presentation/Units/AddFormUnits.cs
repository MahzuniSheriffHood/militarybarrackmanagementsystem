﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.Units
{
    public partial class AddFormUnits : Form
    {
        int id;
        DataLayer.Entity.Units myUnit;
        public AddFormUnits(int id)
        {
            InitializeComponent();
            this.id = id;
            myUnit = (DataLayer.Entity.Units)Facade.GetRecord(id, "Units");

            nameTextBox.Text = myUnit.Name;

            //unittype
            unitTypeCombo.DataSource = Facade.GetDataAsDataTableByRepoName("UnitTypes");
            unitTypeCombo.DisplayMember = "Name";
            unitTypeCombo.ValueMember = "ID";
            unitTypeCombo.SelectedValue = myUnit.UnitTypeID;
            //end unittype

            //parentUnit
            parentUnitCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Units");
            parentUnitCombo.DisplayMember = "Name";
            parentUnitCombo.ValueMember = "ID";
            parentUnitCombo.SelectedValue = myUnit.ParentUnitID;
            //end parentUnit

            //leader
            LeaderCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Soldiers");
            LeaderCombo.DisplayMember = "SoldierName";
            LeaderCombo.ValueMember = "ID";
            LeaderCombo.SelectedValue = myUnit.LeaderID;
            //end leader

            dataGridView1.DataSource = Facade.GetDataAsDataTableByRepoName("UnitsInOperations","getOperationsByUnitID", new object[] { this.id });



        }

        private void AddUnitsForm_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            myUnit.Name = nameTextBox.Text;
            myUnit.LeaderID = LeaderCombo.SelectedValue is null ? 0 : int.Parse(LeaderCombo.SelectedValue.ToString());
            myUnit.ParentUnitID = parentUnitCombo.SelectedValue is null ? 0 : int.Parse(parentUnitCombo.SelectedValue.ToString());
            myUnit.UnitTypeID = int.Parse(unitTypeCombo.SelectedValue.ToString());


            Facade.HandleAddForm(myUnit);

            this.Close();

        }
    }
}
