﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.DisciplinaryPunishments
{
    public partial class AddFormDisciplinaryPunishments : Form
    {
        int id;
        DataLayer.Entity.DisciplinaryPunishments myD;
        public AddFormDisciplinaryPunishments(int id)
        {
            InitializeComponent();
            this.id = id;
            myD = (DataLayer.Entity.DisciplinaryPunishments)Business.Facade.GetRecord(id, "DisciplinaryPunishments");

            //soldier

            soldierCombo.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Soldiers");
            soldierCombo.DisplayMember = "SoldierName";
            soldierCombo.ValueMember = "ID";
            soldierCombo.SelectedValue = myD.AppliedSoldierID;

            //end soldier



            datePicker.Value = myD.Date;


            descRich.Text = myD.Description;




        }

        private void AddDisciplinaryPunishmentsForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            myD.AppliedSoldierID = int.Parse(soldierCombo.SelectedValue.ToString());
            myD.Date = datePicker.Value;
            myD.Description = descRich.Text;

            Business.Facade.HandleAddForm(myD);

            this.Close();


        }
    }
}
