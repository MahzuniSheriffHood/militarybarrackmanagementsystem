﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;
using DataLayer.Entity;
using DataLayer.Repositories;
namespace Presentation.VehicleCategories
{
    public partial class AddFormVehicleCategories : Form
    {
        int id;
        DataLayer.Entity.VehicleCategories myVehicleCategories;
        public AddFormVehicleCategories(int id)
        {
            InitializeComponent();
            this.id = id;
            myVehicleCategories = (DataLayer.Entity.VehicleCategories)Facade.GetRecord(id, "VehicleCategories");
            nameTextBox.Text = myVehicleCategories.Name;

        }

        private void VehicleCategoriesAddForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            myVehicleCategories.Name = nameTextBox.Text;


            Facade.HandleAddForm(myVehicleCategories);

            this.Close();

        }
    }
}
