﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.Operations
{
    public partial class AddFormOperations : Form
    {
        int id;
        DataLayer.Entity.Operations myOperation;
        public AddFormOperations(int id)
        {
            InitializeComponent();
            this.id = id;
            myOperation = (DataLayer.Entity.Operations)Business.Facade.GetRecord(id, "Operations");

            nameTextBox.Text = myOperation.Name;

            timePicker.Format = DateTimePickerFormat.Time;
            timePicker.ShowUpDown = true;
            timePicker.Value = DateTime.Today + myOperation.Time;


            startingDatePicker.Value = myOperation.StartingDate;
            reportDirectoryTextBox.Text = myOperation.ReportDirectory;


            unitCombo.DataSource = Business.Facade.GetDataAsDataTableByRepoName("Units");
            unitCombo.DisplayMember = "Name";
            unitCombo.ValueMember = "ID";




            DataTable myUnits = Business.Facade.GetDataAsDataTableByRepoName("UnitsInOperations", "getUnitsInOrdersByOperationID",new object[] { this.id });
            // unitDataGrid.DataSource = DataLayer.Repositories.UnitsInOperations.getUnitsInOrdersByOperationID(this.id);

            foreach (DataRow item in myUnits.Rows)
            {

                unitDataGrid.Rows.Add(

       new string[]
       {
            item["UnitID"].ToString(),
        item["UnitName"].ToString()
       }

      );
            }

        }

        private void AddOperationsForm_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            myOperation.Name = nameTextBox.Text;
            myOperation.ReportDirectory = reportDirectoryTextBox.Text;
            myOperation.Time = new TimeSpan(timePicker.Value.Hour, timePicker.Value.Minute, timePicker.Value.Second);
            myOperation.StartingDate = startingDatePicker.Value;


            Business.Facade.HandleAddForm(myOperation);


            int operationID = this.id;
            if (operationID == 0)
            {
                DataLayer.Entity.Operations lastOperation = (DataLayer.Entity.Operations)Facade.GetLastRow("Operations");
                Console.WriteLine(lastOperation.ID);
                operationID = lastOperation.ID;
            }
            else operationID = this.id;



            Facade.RemoveUnitsInOperationByOperationID(operationID);
            foreach (DataGridViewRow x in unitDataGrid.Rows)
            {


                DataLayer.Entity.UnitsInOperations myUnit = new DataLayer.Entity.UnitsInOperations();


                myUnit.OperationID = operationID;
                myUnit.UnitID = int.Parse(x.Cells[0].Value.ToString());

                Facade.HandleAddForm(myUnit);

            }


            this.Close();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {

                reportDirectoryTextBox.Text = ofd.FileName;

            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addUnitButton_Click(object sender, EventArgs e)
        {

            int id = int.Parse(unitCombo.SelectedValue.ToString());
            string Name = unitCombo.Text;

            int i = 0;
            foreach (DataGridViewRow x in unitDataGrid.Rows)
            {

                if (x.Cells[0].Value.ToString() == id.ToString())
                {
                    Notification.alert("Unit is already in this operation");
                    i++;
                    break;
                }

                Console.WriteLine("ComboValue -> {0} -- myValue {1}", id.ToString(), x.Cells[0].Value);
            }

            if (i == 0)
            {
                unitDataGrid.Rows.Add(

                    id.ToString(),
                    Name.ToString()

                    );
            }






        }

        private void unitDataGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void unitDataGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            unitDataGrid.Rows.RemoveAt(e.RowIndex);
        }
    }
}
