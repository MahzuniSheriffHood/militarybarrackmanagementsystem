﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataLayer.Repositories;
using DataLayer.Entity;
using Business;

namespace Presentation.WeaponCategories
{
    public partial class AddFormWeaponCategories : Form
    {
        int id;
        DataLayer.Entity.WeaponCategories myWeaponCategories;
        public AddFormWeaponCategories(int id)
        {
            InitializeComponent();

          
            this.id = id;
            this.myWeaponCategories = (DataLayer.Entity.WeaponCategories)Facade.GetRecord(id, "WeaponCategories");
            nameTexBox.Text = myWeaponCategories.Name;
        }

        private void AddWeaponCategoriesForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {


            myWeaponCategories.Name = nameTexBox.Text;
            Facade.HandleAddForm(myWeaponCategories);

            this.Close();
            

        }
    }
}
