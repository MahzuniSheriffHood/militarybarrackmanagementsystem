﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Business;

namespace Presentation.Vehicles
{
    public partial class AddFormVehicles : Form
    {

        int id;
        DataLayer.Entity.Vehicles myVehicle;
        public AddFormVehicles(int id)
        {
            InitializeComponent();
            this.id = id;
            myVehicle = (DataLayer.Entity.Vehicles)Facade.GetRecord(id, "Vehicles");


            //name
            nameTextBox.Text = myVehicle.Name;
            //end name

            //category
            categoryCombo.DataSource = Facade.GetDataAsDataTableByRepoName("VehicleCategories");
            categoryCombo.DisplayMember = "Name";
            categoryCombo.ValueMember = "ID";
            categoryCombo.SelectedValue = myVehicle.VehicleCategoryID;
            //end category

            //belong units
            BelongingUnitCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Units");
            BelongingUnitCombo.DisplayMember = "Name";
            BelongingUnitCombo.ValueMember = "ID";
            BelongingUnitCombo.SelectedValue = myVehicle.BelongingUnitID;
            //end blong units


            //using unit
            usingUnitCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Units");
            usingUnitCombo.DisplayMember = "Name";
            usingUnitCombo.ValueMember = "ID";
            usingUnitCombo.SelectedValue = myVehicle.UsingUnitID;
            //end unsing units

            //status
            statusTextBox.Text = myVehicle.Status;
            //end status

            idNumberTextBox.Text = myVehicle.ID.ToString();


        }

        private void doneButton_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(idNumberTextBox.Text) == 0)
            {
                MessageBox.Show("ID CANNOT BE 0");
                return;
            }

            myVehicle.Name = nameTextBox.Text;
            myVehicle.BelongingUnitID = BelongingUnitCombo.SelectedValue is null ? 0 : int.Parse(BelongingUnitCombo.SelectedValue.ToString());
            myVehicle.Status = statusTextBox.Text;
            myVehicle.VehicleCategoryID = int.Parse(categoryCombo.SelectedValue.ToString());
            myVehicle.UsingUnitID = usingUnitCombo.SelectedValue is null ? 0 : int.Parse(usingUnitCombo.SelectedValue.ToString());

            if (this.id == 0)
            {
                myVehicle.ID = int.Parse(idNumberTextBox.Text);
                Facade.NonAutoIncrementInsert(myVehicle, true);

            }
            else Facade.NonAutoIncrementUpdate(myVehicle, int.Parse(idNumberTextBox.Text));


            this.Close();
            

        }

        private void AddVehiclesForm_Load(object sender, EventArgs e)
        {

        }
    }
}
