﻿namespace Presentation.Vehicles
{
    partial class AddFormVehicles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddFormVehicles));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.BelongingUnitCombo = new System.Windows.Forms.ComboBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.doneButton = new System.Windows.Forms.Button();
            this.categoryCombo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.usingUnitCombo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.idNumberTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "VehicleCategory";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "BelongingUnit";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Status";
            // 
            // BelongingUnitCombo
            // 
            this.BelongingUnitCombo.FormattingEnabled = true;
            this.BelongingUnitCombo.Location = new System.Drawing.Point(119, 91);
            this.BelongingUnitCombo.Name = "BelongingUnitCombo";
            this.BelongingUnitCombo.Size = new System.Drawing.Size(119, 21);
            this.BelongingUnitCombo.TabIndex = 6;
            // 
            // statusTextBox
            // 
            this.statusTextBox.AcceptsReturn = true;
            this.statusTextBox.Location = new System.Drawing.Point(119, 145);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.Size = new System.Drawing.Size(119, 20);
            this.statusTextBox.TabIndex = 8;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(119, 38);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(119, 20);
            this.nameTextBox.TabIndex = 9;
            // 
            // doneButton
            // 
            this.doneButton.Location = new System.Drawing.Point(12, 171);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(226, 23);
            this.doneButton.TabIndex = 10;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = true;
            this.doneButton.Click += new System.EventHandler(this.doneButton_Click);
            // 
            // categoryCombo
            // 
            this.categoryCombo.FormattingEnabled = true;
            this.categoryCombo.Location = new System.Drawing.Point(119, 64);
            this.categoryCombo.Name = "categoryCombo";
            this.categoryCombo.Size = new System.Drawing.Size(119, 21);
            this.categoryCombo.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "UsingUnit";
            // 
            // usingUnitCombo
            // 
            this.usingUnitCombo.FormattingEnabled = true;
            this.usingUnitCombo.Location = new System.Drawing.Point(119, 118);
            this.usingUnitCombo.Name = "usingUnitCombo";
            this.usingUnitCombo.Size = new System.Drawing.Size(119, 21);
            this.usingUnitCombo.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "ID";
            // 
            // idNumberTextBox
            // 
            this.idNumberTextBox.Location = new System.Drawing.Point(119, 12);
            this.idNumberTextBox.Name = "idNumberTextBox";
            this.idNumberTextBox.Size = new System.Drawing.Size(119, 20);
            this.idNumberTextBox.TabIndex = 13;
            // 
            // AddFormVehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 204);
            this.Controls.Add(this.idNumberTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.categoryCombo);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.usingUnitCombo);
            this.Controls.Add(this.BelongingUnitCombo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddFormVehicles";
            this.Text = "New Vehicle";
            this.Load += new System.EventHandler(this.AddVehiclesForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox BelongingUnitCombo;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.ComboBox categoryCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox usingUnitCombo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox idNumberTextBox;
    }
}