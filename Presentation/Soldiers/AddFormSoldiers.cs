﻿using Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation.Soldiers
{
    public partial class AddFormSoldiers : Form
    {
        int id;
        DataLayer.Entity.Soldiers mySoldier;
        public AddFormSoldiers(int id)
        {
            InitializeComponent();
            this.id = id;

            mySoldier = (DataLayer.Entity.Soldiers)Facade.GetRecord(id, "Soldiers");

            firstNameTexBox.Text = mySoldier.FirstName;
            lastNameTextBox.Text = mySoldier.LastName;

            statusTextBox.Text = mySoldier.Status;

            //rank
            rankCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Ranks");
            rankCombo.DisplayMember = "Name";
            rankCombo.ValueMember = "ID";
            rankCombo.SelectedValue = mySoldier.RankID;
            //end rank

            //team
            teamCombo.DataSource = Facade.GetDataAsDataTableByRepoName("Units");
            teamCombo.DisplayMember = "Name";
            teamCombo.ValueMember = "ID";
            teamCombo.SelectedValue = mySoldier.TeamID;
            //end team


            dateOfBirthPicker.Value = mySoldier.DateOfBirth;

            startingDatePicker.Value = mySoldier.StartingDate;


        }

        private void AddSoldiersForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {



            mySoldier.FirstName = firstNameTexBox.Text;
            mySoldier.LastName = lastNameTextBox.Text;

            mySoldier.Status = statusTextBox.Text;

            mySoldier.DateOfBirth = dateOfBirthPicker.Value;
            mySoldier.StartingDate = startingDatePicker.Value;

            mySoldier.RankID = int.Parse(rankCombo.SelectedValue.ToString());
            mySoldier.TeamID = int.Parse(teamCombo.SelectedValue.ToString());

            Facade.HandleAddForm(mySoldier);

            this.Close();

        }

        private void startingDatePicker_ValueChanged(object sender, EventArgs e)
        {


            

        }
    }
}
