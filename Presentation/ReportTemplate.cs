﻿using DataLayer;
using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Presentation
{
    public partial class ReportTemplate : Form
    {
        public ReportTemplate(string ReportName)
        {
            InitializeComponent();
            reportName = ReportName;
        }

        string reportName;

        private void ReportTemplate_Load(object sender, EventArgs e)
        {
            string tableName = reportName.Split('-')[0];
            this.Text = tableName + " Report";
            MilitaryDataSets mds = new MilitaryDataSets();
            Business.Facade.FillMilitaryDataSet(mds, tableName);
            reportViewer1.ProcessingMode = ProcessingMode.Local;
            LocalReport localReport = reportViewer1.LocalReport;
            localReport.ReportPath = @"Reports\" + reportName + ".rdlc";
            ReportDataSource dataSource = new ReportDataSource(tableName+"DataSet", mds.Tables[mds.Tables.IndexOf(tableName)]);
            reportViewer1.LocalReport.DataSources.Clear();
            localReport.DataSources.Add(dataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
