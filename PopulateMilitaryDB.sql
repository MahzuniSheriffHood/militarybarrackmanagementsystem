use Military
insert into Ranks values('Er'), ('Onba��'), ('Astsubay')
insert into UnitTypes values('Tim'), ('Manga'), ('Tak�m'), ('B�l�k'),('Tabur'), ('Alay'), ('Tugay'), ('T�men'), ('Kolordu'), ('Ordu')
insert into Units values(10, NULL, '1. Ordu', NULL), (9, 1, '1. Kolordu', NULL), (8, 2, '1. T�men', NULL), (7, 3, '1. Tugay', NULL), (6, 4, '1. Alay', NULL), (5, 5, '1. Tabur', NULL), 
(4, 6, '1. B�l�k', NULL), (3, 7, '1. Tak�m', NULL), (2, 8, '1. Manga', NULL), (1, 9, '1. Tim', NULL), (5, 5, '2. Tabur', NULL), (5, 5, '3. Tabur', NULL)
insert into Soldiers values('Burak', 'Avc�', 'G�revde', 1, 1, convert(datetime,'05.01.1998', 104), convert(datetime,'23.06.2023', 104)), ('Can', 'Azizo�lu', 'G�revde', 1, 1, convert(datetime,'12.07.1998', 104), convert(datetime,'23.06.2022', 104)), ('�mer', 'Ayd�n', 'G�revde', 1, 1, convert(datetime,'31.12.1997', 104), convert(datetime,'13.04.2045', 104))
Update Units set LeaderID = 2 where ID = 10
insert into WeaponCategories values('Kesici'), ('Sald�r� T�fe�i'), ('Pompal�'), ('Tabanca'), ('Roketatar')
insert into Arsenal values('5.45x18', 10000), ('7.63x25', 51468), ('9x18', 125487), ('7.62x51', 46548), ('56x45', 923531), ('PG-29V', 500000), ('M18 K�rm�z�', 78216)
insert into Weapons values(54846, 'H&K G36K', 2, 3, '��levsel'), (19458, 'FN F2000', 2, NULL, 'Servis D���'), (89745, 'SIG Sauer P250', 4, NULL, '��levsel'), (353687, 'M9 Phrobis III', 1, 3, '��levsel')
insert into DisciplinaryPunishments values(3, convert(datetime, '11.12.2048', 104), 'Kantinde "Oha-Diyorum" izlerken yakalanmas�n�n sonucunda 3 saat i�inde 5000 ��nav �ekmesi uygun g�r�lm��t�r')
insert into VehicleCategories values('F�ze Rampas�'), ('Z�rhl� Personel Arac�'), ('Tank'), ('Avc� U�a��')
insert into Vehicles values (54654, 'S-400', 1, 3, 4, '��levsel'), (7899, 'MRAP', 2, NULL, NULL, 'Bak�mda'), (789879, 'F-22 Raptor', 4, NULL, NULL, '��levsel')
insert into Operations values ('Kara Buz', convert(datetime,'23.06.2025', 104), '06:00:00', 'C:\RaporKaraBuz.rap'), ('Denizkabu�u', convert(datetime,'24.06.2025', 104), '06:00:00', 'C:\RaporDenizkabu�u.rap')
insert into UnitsInOperations values (1,10), (2,6), (2,11)
insert into ArsenalUsages values (10, 3, 5000, 2), (10, 5, 5000, 2), (10, 6, 15, 1)
insert into Equipments values (465321, 'GVN Angled Foregrip', 2, '��levsel'), (465322, 'GVN Angled Foregrip', 3, 'Bak�mda'), (155879, 'Trijicon ACOG 4x32 Scope', 2, 'Servis D���')
