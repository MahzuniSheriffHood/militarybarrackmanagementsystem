﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Arsenal
    {


        public int ID { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

    }
}
