﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Vehicles
    {

        public int ID { get; set; }
        public string Name { get; set; }

        public int VehicleCategoryID { get; set; }

        public int BelongingUnitID { get; set; }

        public int UsingUnitID { get; set; }

        public string  Status { get; set; }


    }
}
