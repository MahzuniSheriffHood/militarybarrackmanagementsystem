﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Soldiers
    {

        public int ID { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Status { get; set; }
        public int RankID { get; set; }

        public int TeamID { get; set; }

        public DateTime DateOfBirth { get; set; } = DateTime.Now;

        public DateTime StartingDate { get; set; } = DateTime.Now;
    }
}
