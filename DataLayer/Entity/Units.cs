﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
   public class Units
    {


        public int ID { get; set; }
        public int UnitTypeID{ get; set; }

        public int ParentUnitID { get; set; }

        public string Name { get; set; }

        public int LeaderID { get; set; }

    }
}
