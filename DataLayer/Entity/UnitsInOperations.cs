﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
   public class UnitsInOperations
    {


        public int ID { get; set; } = 0;
        public int OperationID { get; set; }
        public int UnitID { get; set; }
    }
}
