﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class WeaponCategories
    {

        public int ID { get; set; }

        public string Name { get; set; }
    }
}
