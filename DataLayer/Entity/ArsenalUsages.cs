﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class ArsenalUsages
    {

        public int ID { get; set; } = 0;

        public int Quantity { get; set; }

        public int UseDUnitID { get; set; }

        public int UsedArsenalID { get; set; }

        public int OperationID { get; set; }

    }
}
