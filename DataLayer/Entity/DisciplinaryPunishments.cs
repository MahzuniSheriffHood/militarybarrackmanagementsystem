﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class DisciplinaryPunishments
    {

        public  int ID { get; set; }

        public int AppliedSoldierID { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;

        public string Description { get; set; }



    }
}
