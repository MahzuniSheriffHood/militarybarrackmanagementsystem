﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    public class Operations
    {

        public int ID { get; set; }

        public string Name { get; set; }

        public DateTime StartingDate { get; set; } = DateTime.Now;


        public TimeSpan Time { get; set; }

        public string ReportDirectory { get; set; }

    }
}
