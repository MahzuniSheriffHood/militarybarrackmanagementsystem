﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
   public class Equipments
    {


        public int ID { get; set; }
        public string Name { get; set; }
        public int BelongingSoldierID { get; set; }

        public string Status { get; set; }
    }
}
