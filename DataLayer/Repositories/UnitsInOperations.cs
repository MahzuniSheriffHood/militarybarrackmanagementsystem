﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    public class UnitsInOperations : SpecialTreatTable<UnitsInOperations>
    {
        public static void removeUnitsInOperationByOperationID(int id)
        {
            SqlCommand command = new SqlCommand("removeUnitsInOperationByOperationID", getConnection());
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OperationID", id);
            command.ExecuteNonQuery();
        }
        public static DataTable getUnitsInOrdersByOperationID(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter("getUnitsInOrdersByOperationID", connection);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@OperationID", id);
            DataTable myDataTable = new DataTable();
            adapter.Fill(myDataTable);
            return myDataTable;
        }
        public static DataTable getOperationsByUnitID(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter("getOperationsByUnitID", connection);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@UnitID", id);
            DataTable myDataTable = new DataTable();
            adapter.Fill(myDataTable);
            return myDataTable;
        }
    }
}
