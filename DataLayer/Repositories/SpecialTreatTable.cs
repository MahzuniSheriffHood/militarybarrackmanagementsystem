﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    public class SpecialTreatTable<T> : CoreTables<T>
    {
        public static DataTable Select()
        {
            SqlDataAdapter adapter = new SqlDataAdapter(typeof(T).Name + "Select", connection);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable myDataTable = new DataTable();
            adapter.Fill(myDataTable);
            return myDataTable;
        }
    }
}
