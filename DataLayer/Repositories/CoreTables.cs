﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Data;
using System.Diagnostics;
using System.Reflection;

namespace DataLayer.Repositories
{
    public  class CoreTables<T>
    {

        protected static SqlConnection connection = DatabaseHandler.connection;

        public static SqlConnection getConnection()
        {
            if (connection.State != ConnectionState.Open) connection.Open();
            return connection;
        }

        public static void UpdateOrInsertChooser(object myEntity,bool isIDParamIsPrimaryComesFromHisEntity=false)
        {
            

            int ID = (int)myEntity.GetType().GetProperty("ID").GetValue(myEntity, null);

            if (ID != 0)
            {
                //update
                Update(myEntity);

            }
            else
            {
                //insert
                Insert(myEntity, isIDParamIsPrimaryComesFromHisEntity);
            }


        }

        public static void Insert(object myEntity, bool isIDParamIsPrimaryComesFromHisEntity = false)
        {

            SqlCommand command = new SqlCommand(typeof(T).Name + "Insert", connection);
            command.CommandType = CommandType.StoredProcedure;

            foreach (var x in myEntity.GetType().GetProperties())
            {

                if(!isIDParamIsPrimaryComesFromHisEntity)
                { 
               if (x.Name == "ID") continue;    //except columns
                }
                Console.WriteLine(x.Name+" -> "+x.ToString());
                command.Parameters.AddWithValue("@" + x.Name, myEntity.GetType().GetProperty(x.Name).GetValue(myEntity, null));

            }

            command.Connection = getConnection();
            command.ExecuteNonQuery();

        }



       


        public static void Update(object myEntity,int alternativeID=0)
        {

            SqlCommand command = new SqlCommand(typeof(T).Name + "Update", connection);
            command.CommandType = CommandType.StoredProcedure;


            foreach (var x in myEntity.GetType().GetProperties())
            {
                Console.WriteLine(x.Name);
                command.Parameters.AddWithValue("@" + x.Name, myEntity.GetType().GetProperty(x.Name).GetValue(myEntity, null));

            }
            if (alternativeID != 0) command.Parameters.AddWithValue("@alternativeID", alternativeID);

           command.Connection = getConnection();
            command.ExecuteNonQuery();
        }

        public static dynamic LastRow()
        {
            SqlCommand command = new SqlCommand("getLastRowFromTable", connection);
            command.CommandType = CommandType.StoredProcedure;

            
          
            command.Parameters.AddWithValue("@TableName", typeof(T).Name);
            command.Connection = getConnection();
            SqlDataReader dr = command.ExecuteReader();

            object x = Activator.CreateInstance(Type.GetType("DataLayer.Entity." + typeof(T).Name));


            while (dr.Read()) //this shit is one shot deal
            {
                foreach (var y in x.GetType().GetProperties())
                {


                    PropertyInfo propInfo = x.GetType().GetProperty(y.Name);
                    Console.WriteLine("DR -> " + dr[y.Name]);
                    propInfo.SetValue(x, Convert.ChangeType(dr[y.Name], propInfo.PropertyType), null);

                }

            }

            dr.Close();

            if ((int)(x.GetType().GetProperty("ID").GetValue(x, null)) != 0)
            {
                return x;
            }
            else
            {
                object newX = Activator.CreateInstance(Type.GetType("DataLayer.Entity." + typeof(T).Name));

                return newX;
            }

        }


        public static dynamic First(int ID)
        {
            SqlCommand command = new SqlCommand("TableFirst", connection);
            command.CommandType = CommandType.StoredProcedure;

            Console.WriteLine(ID);
            command.Parameters.AddWithValue("@ID", ID);
            command.Parameters.AddWithValue("@TableName", typeof(T).Name);
            command.Connection = getConnection();
            SqlDataReader dr = command.ExecuteReader();
            
            object x = Activator.CreateInstance(Type.GetType("DataLayer.Entity." + typeof(T).Name));


            while (dr.Read()) //this shit is one shot deal
            {
                foreach (var y in x.GetType().GetProperties())
                {
                   

                    PropertyInfo propInfo = x.GetType().GetProperty(y.Name);
                    Console.WriteLine("DR -> " + dr[y.Name]);
                    var myData = dr[y.Name];
                    if (myData == DBNull.Value)
                    {
                        myData = 0;
                    }
                   
                    propInfo.SetValue(x, Convert.ChangeType(myData, propInfo.PropertyType), null);
                    
                }

            }

            dr.Close();

            if ((int)(x.GetType().GetProperty("ID").GetValue(x, null)) != 0)
            {
                return x;
            }
            else
            {
                object newX = Activator.CreateInstance(Type.GetType("DataLayer.Entity." + typeof(T).Name));

                return newX;
            }

        }

    }
}
