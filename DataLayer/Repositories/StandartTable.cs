﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
    public class StandartTable<T> : CoreTables<T>
    {
        public static DataTable Select()
        {
            SqlDataAdapter adapter;
            adapter = new SqlDataAdapter("TableSelect", DatabaseHandler.connection);
            adapter.SelectCommand.Parameters.AddWithValue("@TableName", typeof(T).Name);
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable myDataTable = new DataTable();
            adapter.Fill(myDataTable);
            return myDataTable;
        }
    }
}
