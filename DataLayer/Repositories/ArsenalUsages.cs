﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Repositories
{
   public class ArsenalUsages : SpecialTreatTable<ArsenalUsages>
    {


        public static DataTable ArsenalUsagesByOprationID(int id)
        {
            SqlDataAdapter adapter = new SqlDataAdapter("ArsenalUsagesByOperationID", getConnection());
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
            adapter.SelectCommand.Parameters.AddWithValue("@OperationID", id);
            DataTable myDataTable = new DataTable();
            adapter.Fill(myDataTable);
            return myDataTable;
        }

        public static void removeArsenalUsagesByOperationID(int id)
        {

            SqlCommand command = new SqlCommand("removeArsenalUsagesByOperationID", getConnection());
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OperationID", id);
            command.ExecuteNonQuery();
            
        }


        public static DataTable SelectGroupByOperations()
        {
            SqlDataAdapter adapter = new SqlDataAdapter("SelectGroupByOperations", getConnection());
            adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            DataTable myDataTable = new DataTable();
            adapter.Fill(myDataTable);
            return myDataTable;
        }
    }
}
