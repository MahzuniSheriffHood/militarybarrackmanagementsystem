﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Business
{
    public static class Facade
    {
        public static bool Authenticate(string Username, string Password)
        {
            return DatabaseHandler.TryConnection(Username, Password);
        }

        public static void RemoveUnitsInOperationByOperationID(int id)
        {
            DataLayer.Repositories.UnitsInOperations.removeUnitsInOperationByOperationID(id);
        }

        public static void NonAutoIncrementInsert(object entity, bool isIDParamIsPrimaryComesFromHisEntity = false)
        {
            RepositoryHandler.ExecuteMethodInARepo(entity.GetType().Name, "Insert", new object[] { entity, isIDParamIsPrimaryComesFromHisEntity });
        }

        public static void NonAutoIncrementUpdate(object entity, int ID)
        {
            RepositoryHandler.ExecuteMethodInARepo(entity.GetType().Name, "Update", new object[] { entity, ID });
        }

        public static void RemoveArsenalUsageByOperationID(int id)
        {
            DataLayer.Repositories.ArsenalUsages.removeArsenalUsagesByOperationID(id);
        }

        public static void HandleAddForm(object entity, bool isIDParamIsPrimaryComesFromHisEntity = false)
        {
            RepositoryHandler.ExecuteMethodInARepo(entity.GetType().Name, "UpdateOrInsertChooser", new object[] { entity, isIDParamIsPrimaryComesFromHisEntity });
        }

        public static object GetLastRow(string tableName)
        {
            return RepositoryHandler.ExecuteMethodInARepo(tableName, "LastRow", null);
        }

        public static object GetRecord(int id, string tableName)
        {
            return RepositoryHandler.ExecuteMethodInARepo(tableName, "First", new object[] { id });
        }

        public static void FillMilitaryDataSet(MilitaryDataSets mds, string tableName)
        {
            DataTable dt = (DataTable)RepositoryHandler.ExecuteMethodInARepo(tableName, "Select");
            foreach (DataRow dr in dt.Rows)
            {
                mds.Tables[tableName].ImportRow(dr);
            }
        }
        public static DataTable GetDataAsDataTableByRepoName(string Name, string alternativeStoreProcName = null, object[] Parameters = null)
        {
            return (DataTable)RepositoryHandler.ExecuteMethodInARepo(Name, alternativeStoreProcName ?? "Select", Parameters);
        }
    }
}
