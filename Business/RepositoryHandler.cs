﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    class RepositoryHandler
    {
        internal static object ExecuteMethodInARepo(string RepositoryName, string MethodName, object[] Parameters = null)
        {
            Type type = Type.GetType("DataLayer.Repositories." + RepositoryName + ",DataLayer");
            while (type.GetMethod(MethodName) == null)
            {
                type = type.BaseType;
            }
            return type.GetMethod(MethodName).Invoke(null, Parameters);
        }
    }
}
